"use strict";

const express = require("express");
const app = express();
const port = 1208;

app.use("/", express.static("public"));

app.get("*", (req, res) => {
    res.status(404)
    res.send("Not Found");
});

app.listen(
    port,
    () => {
        console.log(`Server listening on PORT ${port}`);
    }
);